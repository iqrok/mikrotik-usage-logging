const mtikConfig = require('./config/mikrotik_config.json');
const RouterOSClient = require('routeros-client').RouterOSClient;

const getListDHCP = async function(){
	const api = new RouterOSClient(mtikConfig);

	const mtikResult = await api.connect()
		.then(async (client) => {
				let status = false
				return client.menu("/ip dhcp-server lease")
				.exec('print')
				.then(results => {
					//~ console.log('DHCP',results);
					return results;
				})
				.catch(error=>{
					console.error(error);
				});
		})
		.catch((err) => {
				// Connection error
				console.error('Timed Out Error:',err);
				return null;
		});

	await api.close().then(() =>{
		console.log('DHCP DONE..!');
	}).catch((err) =>{
		// Error trying to disconnect
		console.log('DHCP Err:',err);
	});

	return mtikResult;
}

module.exports = {
	getListDHCP:getListDHCP,
};
