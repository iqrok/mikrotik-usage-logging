const config = require('./config/db_config.json');
const mtikConfig = require('./config/mikrotik_config.json');

const __mysql = require('./class/db.class.js');
const mysql = new __mysql(config);

const RouterOSClient = require('routeros-client').RouterOSClient;
const CronJob = require('cron').CronJob;

const __INTERVAL = 10000;
const localIP_LIKES = '192.168.';

const cronUpdate = require('./updateData.js');
const dhcp = require('./dhcp-server.js');

const getDataMikrotik = async function(){
	const api = new RouterOSClient(mtikConfig);

	const mtikResult = await api.connect()
		.then(async (client) => {
				let status = false
				await client.menu("/ip accounting snapshot")
				.exec('take')
				.then(result => {
					status = true;
					return result;
				})
				.catch(error=>{
					console.error(error);
				});

				if(status){
					const tmpMtik = await client.menu("/ip accounting snapshot")
					.exec('print')
					.then((result) => {
						return result;
					}).catch((err) => {
						console.log(err); // Some error trying to get the identity
						return null;
					});

					return tmpMtik;
				}
		})
		.catch((err) => {
				// Connection error
				console.error('Timed Out Error:',err);
				return null;
		});

	await api.close().then(() =>{
		console.log('Fetching DONE..!');
	}).catch((err) =>{
			// Error trying to disconnect
			console.log('Fetching Err:',err);
	});

	return mtikResult;
}

const insertIntoDB = async function(){
	const timer = new Date();
	const mtikResult = await getDataMikrotik();

	if(mtikResult){
		let sql = 'INSERT INTO mtik_accounting(`accSrcAddr`,`accDstAddr`,`accPackets`,`accBytes`,`accTypeId`) VALUES ?';
		let params = [];

		for(let key in mtikResult){
			const data = mtikResult[key];

			/* NETWORK TYPE:
			 *	1 : LOCAL
			 * 	2	:	DOWNSTREAM
			 * 	3	:	UPSTREAM
			 * */

			let networkType = null;
			if(data.srcAddress.includes(localIP_LIKES) && data.dstAddress.includes(localIP_LIKES))
				networkType = 1;
			else if(!data.srcAddress.includes(localIP_LIKES) && data.dstAddress.includes(localIP_LIKES))
				networkType = 2;
			else if(data.srcAddress.includes(localIP_LIKES) && !data.dstAddress.includes(localIP_LIKES))
				networkType = 3;
			else
				networkType = 4;

			params.push([data.srcAddress,data.dstAddress,data.packets,data.bytes,networkType]);
		}

		return mysql.query(sql,[params])
			.then(result => {
				console.log(new Date()-timer,'SUCCESS ',params.length,new Date());
				return true;
			})
			.catch(async error=>{
				if(error.code == 'ER_LOCK_DEADLOCK'){
					console.log('DEADELOCK HAPPENS. Trying to Re-query again');
					await mysql.query(sql,[params])
						.catch(error => {
							console.error('Still Error after deadlock',error);
						})
				}
				else{
					console.error(error);
				}

				return false;
			});
	}

	console.log('Failed while attempting fetch');
	return false
}

const updateDHCP = new CronJob('* 2,32 * * * *', async function() {
	const sql = "INSERT INTO mtik_local_ip(`ipAddress`,`ipHostname`,`ipMACAddress`) VALUES(?,?,?) ON DUPLICATE KEY UPDATE  ipMACAddress=VALUES(ipMACAddress)";

	const DHCPList = await dhcp.getListDHCP();
	//~ console.log(DHCPList);
	for(let tmp of DHCPList){
		await mysql.query(sql,[tmp.address ,tmp.hostName ,tmp.macAddress])
			.then(result => {
				console.log('DHCP LIST UPDATE SUCCEEDED',new Date());
				return true;
			})
			.catch(error => {
				console.error('DHCP LIST  UPDATE FAILED! ',error);
				return false;
			});
	}
}, null, true, 'Asia/Jakarta');

cronUpdate.start();

setInterval(async function(){
	await insertIntoDB();
}, __INTERVAL);
