-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 07, 2019 at 07:20 PM
-- Server version: 10.3.17-MariaDB-0+deb10u1
-- PHP Version: 7.3.9-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mikrotik_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `mtik_accounting`
--

CREATE TABLE `mtik_accounting` (
  `accId` bigint(20) NOT NULL,
  `accSrcAddr` varchar(15) DEFAULT NULL,
  `accDstAddr` varchar(15) DEFAULT NULL,
  `accPackets` int(11) DEFAULT NULL,
  `accBytes` int(11) DEFAULT NULL,
  `accTypeId` tinyint(4) UNSIGNED DEFAULT NULL,
  `accTimestamp` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mtik_local_ip`
--

CREATE TABLE `mtik_local_ip` (
  `ipAddress` varchar(16) NOT NULL,
  `ipHostname` varchar(64) DEFAULT NULL,
  `ipMACAddress` varchar(24) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mtik_total_downstream_local`
--

CREATE TABLE `mtik_total_downstream_local` (
  `downstreamIP` varchar(15) NOT NULL,
  `downstreamDate` date NOT NULL,
  `downstreamTotalMB` int(11) DEFAULT NULL,
  `downstreamTimestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mtik_total_upstream_local`
--

CREATE TABLE `mtik_total_upstream_local` (
  `upstreamIP` varchar(15) NOT NULL,
  `upstreamDate` date NOT NULL,
  `upstreamTotalMB` int(11) DEFAULT NULL,
  `upstreamTimestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mtik_types`
--

CREATE TABLE `mtik_types` (
  `typeId` tinyint(4) UNSIGNED NOT NULL,
  `typeName` varchar(10) DEFAULT NULL,
  `typeNetwork` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mtik_accounting`
--
ALTER TABLE `mtik_accounting`
  ADD PRIMARY KEY (`accId`),
  ADD KEY `accType` (`accTypeId`),
  ADD KEY `accSrcAddr` (`accSrcAddr`),
  ADD KEY `accDstAddr` (`accDstAddr`);

--
-- Indexes for table `mtik_local_ip`
--
ALTER TABLE `mtik_local_ip`
  ADD PRIMARY KEY (`ipAddress`);

--
-- Indexes for table `mtik_total_downstream_local`
--
ALTER TABLE `mtik_total_downstream_local`
  ADD PRIMARY KEY (`downstreamIP`,`downstreamDate`);

--
-- Indexes for table `mtik_total_upstream_local`
--
ALTER TABLE `mtik_total_upstream_local`
  ADD PRIMARY KEY (`upstreamIP`,`upstreamDate`);

--
-- Indexes for table `mtik_types`
--
ALTER TABLE `mtik_types`
  ADD PRIMARY KEY (`typeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mtik_accounting`
--
ALTER TABLE `mtik_accounting`
  MODIFY `accId` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mtik_types`
--
ALTER TABLE `mtik_types`
  MODIFY `typeId` tinyint(4) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
