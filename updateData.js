const config = require('./config/db_config.json');
const mtikConfig = require('./config/mikrotik_config.json');

const __mysql = require('./class/db.class.js');
const mysql = new __mysql(config);

const CronJob = require('cron').CronJob;

const updateData = new CronJob('55 29,59,14,44 * * * *', async function() {
	const upstream = mysql.query('CALL  p_insert_edit_total_upstream();',[])
		.then(result => {
			console.log('UPSTREAM UPDATE SUCCEEDED',new Date());
			return true;
		})
		.catch(error => {
			console.error('UPSTREAM UPDATE FAILED! ',error);
			return false;
		});

	const downstream = mysql.query('CALL  p_insert_edit_total_downstream();',[])
		.then(result => {
			console.log('DOWNSTREM UPDATE SUCCEEDED',new Date());
			return true;
		})
		.catch(error => {
			console.error('DOWNSTREM UPDATE FAILED! ',error);
			return false;
		});

	await(downstream);
	await(upstream);
}, null, true, 'Asia/Jakarta');

module.exports = updateData;
